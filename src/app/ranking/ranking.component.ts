import { Component, OnInit } from '@angular/core';
import { RankingService } from '../shared/services/ranking.service';
import { staggeredTrigger } from './animations';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.scss'],
  animations: [
    staggeredTrigger
  ]
})
export class RankingComponent implements OnInit {

  scoresList: any[] = [];
  filteredScoreList: any[] = [];
  perPage = 10;
  currentRankingPage = -1;
  maxPages = 0;

  previousActive = false;
  nextActive = false;
  currentUserId;

  constructor( private rankingService: RankingService, private auth: AuthService ) { }

  ngOnInit() {
    this.currentUserId = this.auth.getUser()._id;
    this
      .rankingService
      .fetchScores()
      .subscribe(
        results => {
        this.scoresList = results.data;
        this.maxPages = Math.max( 1, Math.ceil( this.scoresList.length / this.perPage ) );
        this.onChangePage( 'n' );
      }
    );
  }

  onChangePage( direction: string ) {
    if ( direction === 'n' && this.currentRankingPage < this.maxPages - 1 ) {
      const start = ++this.currentRankingPage * this.perPage;
      const end = start + this.perPage;
      this.filteredScoreList = this.scoresList.slice( start, end );
      this.fillResultsPage();
    } else if ( direction === 'p'  && this.currentRankingPage > 0 ) {
      const start = --this.currentRankingPage * this.perPage;
      const end = start + this.perPage;
      this.filteredScoreList = this.scoresList.slice( start, end );
    }

    this.handleButtons();
  }

  handleButtons() {
    this.nextActive = this.currentRankingPage < this.maxPages - 1;
    this.previousActive = this.currentRankingPage > 0;
  }

  fillResultsPage() {
    let counter = this.filteredScoreList.length;
    while ( counter < this.perPage ) {
      this.filteredScoreList.push( {} );
      counter++;
    }
  }

  public isEmpty( item ): boolean {
    return ( item && (Object.keys( item ).length === 0));
  }

}
