import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  loginError = false;

  constructor( private authService: AuthService, private  router: Router ) {}

  ngOnInit() {
    this.loginForm = new FormGroup({
      email: new FormControl('', {
        validators: [
          Validators.required,
          Validators.email
        ]
      }),
      password: new FormControl('', {
        validators: [
          Validators.required
        ]
      })
    });
  }

  onSubmit() {
    this
      .authService
      .login({
        email: this.loginForm.value.email,
        password: this.loginForm.value.password
      })
      .subscribe(
        ( res ) => {
          if ( res ) {
            return this.router.navigate([ '/start' ] );
          }
        },
        error => {
          this.loginError = true;
          console.error( error );
        },
        () => console.log( 'Successfully registered and logged in! ' )
      );
  }
}
