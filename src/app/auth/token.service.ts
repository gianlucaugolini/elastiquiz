import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  private localStorageKey = 'access_token';

  constructor() { }

  getToken() {
    return localStorage.getItem( this.localStorageKey );
  }

  setToken( token: string ) {
    localStorage.setItem( this.localStorageKey, token );
  }

  removeToken() {
    localStorage.removeItem( this.localStorageKey );
  }

  checkToken() {
    const token = this.getToken();
    return !token ? false : token;
  }

  tokenExpired() {
    const token = this.checkToken();

    if ( !token ) {
      return true;
    }
    return new JwtHelperService().isTokenExpired( token );
  }

  decodeUser(): User {
    const decodedUser = new JwtHelperService()
      .decodeToken( this.getToken() )
      .userdata;

    return new User( decodedUser.id,  decodedUser.name, decodedUser.email );
  }

  /*
  payload( token: string ) {
    const payload = token.split( '.' )[1];
    return this.decode( payload );
  }

  decode( payload ) {
    return JSON.parse( atob( payload ) );
  }
*/
}
