export class User {

  constructor( private id: number, public name: string, private email: string) {}

  get _id() {
    return this.id;
  }
  get emailAddress() {
    return this.email;
  }
}
