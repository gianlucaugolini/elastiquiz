import { User } from './user.model';
import { AuthData } from './auth-data.model';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from './token.service';

@Injectable()
export class AuthService {

  private prodBaseUrl = 'http://elastiquiz-api.pluriahost.it/public/api/auth';
  private devBaseUrl = 'http://localhost:8000/api/auth';

  private endpoints = {
    register  : '/register',
    login     : '/login',
    logout    : '/logout',
    user      : '/user',
    me        : '/me',
    refresh   : '/refresh',
  };


  authChange$ = new BehaviorSubject<User>( null );
  private user: User;

  constructor( private http: HttpClient, private tokenService: TokenService ) {
    this.checkLoggedUser();
  }


  login( credentials: AuthData ) {
    const url = this.prodBaseUrl + this.endpoints.login;
    return this.http.post<any>( url, credentials )
      .pipe(
        map(res => {
            if ( res && res.access_token ) {
              this.tokenService.setToken( res.access_token );
              this.user = this.tokenService.decodeUser();
              this.authChange$.next( this.user );
              return true;
            }
            return false;
          }
        )
      );
  }


  register( credentials: AuthData ) {
    const url = this.prodBaseUrl + this.endpoints.register;
    return this.http.post<any>( url, credentials )
      .pipe(
        map(res => {
            if ( res && res.token ) {
              console.log( res.token );
              this.tokenService.setToken( res.token );
              this.user = this.tokenService.decodeUser();
              this.authChange$.next( this.user );
              return true;
            }
            return false;
          }
        )
      );
  }

  isLoggedIn() {
    return !this.tokenService.tokenExpired();
  }

  getUser() {
    return this.user;
  }

  logout() {
    const url = this.prodBaseUrl + this.endpoints.logout;

    const options = this.prepareHeaders();
    return this.http.get<any>( url, options )
      .pipe(
        tap(res => {
            console.log( 'on logout: ', res );
            this.tokenService.removeToken();
            this.user = null;
            this.authChange$.next( this.user );
          }
        )
      );
  }

  checkLoggedUser() {
    const isExpired = this.tokenService.tokenExpired();

    if ( !isExpired ) {
      this.user = this.tokenService.decodeUser();
      this.authChange$.next( this.user );
    }
  }

  private prepareHeaders() {

    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.tokenService.getToken()
      })
    };

  }

}
