import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AuthService } from '../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registrationError = false;

  constructor( private authService: AuthService, private  router: Router ) { }

  ngOnInit() { }

  onSubmit( form: NgForm ) {
    console.log( form );
    this.authService.register({
      name: form.value.name,
      email: form.value.email,
      password: form.value.password
    })
      .subscribe(
        res => {
          if ( res ) {
            this.router.navigate( [ '/start' ] );
          }
        },
        error => {
          console.error( error );
          this.registrationError = JSON.parse( error.error );

          console.log( this.registrationError );
        },
            () => console.log( 'Successfully registered and logged in! ' )
      );
  }

}
