import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { routingModule } from './app.routing';
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { QuizComponent } from './quiz/quiz.component';
import { QuizTopComponent } from './quiz/quiz-top/quiz-top.component';
import { QuizContentComponent } from './quiz/quiz-content/quiz-content.component';
import { RankingComponent } from './ranking/ranking.component';
import { QuestionComponent } from './quiz/quiz-content/question/question.component';
import { AnswersBlockComponent } from './quiz/quiz-content/question/answers-block/answers-block.component';
import { AnswerComponent } from './quiz/quiz-content/question/answers-block/answer/answer.component';
import { QuizService } from './shared/services/quiz.service';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from './shared/services/data.service';

import { ScorePanelComponent } from './quiz/quiz-top/score-panel/score-panel.component';
import { TimerComponent } from './quiz/quiz-top/timer/timer.component';
import { DecodedHtmlPipe } from './shared/pipes/decoded-html.pipe';
import { StartComponent } from './start/start.component';
import { CanPlayGuard } from './shared/guards/can-play.guard';
import { QuizResultComponent } from './quiz/quiz-result/quiz-result.component';
import { HasCompletedGuard } from './shared/guards/has-completed.guard';
import { AuthService } from './auth/auth.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthGuard } from './auth/auth.guard';
import { RankingService } from './shared/services/ranking.service';
import { LoggedInGuard } from './auth/logged-in.guard';
import { LogoComponent } from './home/logo/logo.component';
import { TokenService } from './auth/token.service';
import { HeaderComponent } from './ui/header/header.component';
import { ButtonComponent } from './ui/button/button.component';
import { ProgressComponent } from './quiz/quiz-top/progress/progress.component';
import { GuestBookComponent } from './guest-book/guest-book.component';
import { FeedbackService } from './shared/services/feedback.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NotFoundComponent,
    LoginComponent,
    RegisterComponent,
    QuizComponent,
    QuizTopComponent,
    QuizContentComponent,
    RankingComponent,
    QuestionComponent,
    AnswerComponent,
    AnswersBlockComponent,
    StartComponent,
    ScorePanelComponent,
    TimerComponent,
    DecodedHtmlPipe,
    QuizResultComponent,
    LogoComponent,
    HeaderComponent,
    ButtonComponent,
    ProgressComponent,
    GuestBookComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    routingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    DataService,
    QuizService,
    AuthService,
    TokenService,
    AuthGuard,
    LoggedInGuard,
    DecodedHtmlPipe,
    CanPlayGuard,
    HasCompletedGuard,
    RankingService,
    FeedbackService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
