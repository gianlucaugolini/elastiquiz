import { Component, OnInit } from '@angular/core';
import { swTrigger } from './animations';
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import {FeedbackService} from '../shared/services/feedback.service';

@Component({
  selector: 'app-guest-book',
  templateUrl: './guest-book.component.html',
  styleUrls: ['./guest-book.component.scss'],
  animations: [
    swTrigger
  ]
})
export class GuestBookComponent implements OnInit {

  feedbackForm: FormGroup;
  swee = false;
  selectedRating = 0;
  formSubmitted = false;

  constructor( private feedbackService: FeedbackService ) { }

  ngOnInit() {
    this.feedbackForm = new FormGroup({
      rating: new FormControl('', {
        validators: [
          Validators.required
        ]
      }),
      feedback: new FormControl('', {
        validators: [
          Validators.required
        ]
      })
    });
  }

  onSendFeedback() {
    this.feedbackService.saveFeedback( this.feedbackForm.value.rating, this.feedbackForm.value.feedback )
      .subscribe(
        res => {
          if ( res ) {

            if ( res.saved === true ) {
              this.formSubmitted = true;
            }
            if ( res.ee === true ) {
              this.swee = true;
            }
          }
        },
        error => {
          console.error( error );
          // this.registrationError = JSON.parse( error.error );

          // console.log( this.registrationError );
        },
        () => console.log( 'Feedback saved!' )
      );
  }

  setRating( rating: number ) {

    this.selectedRating = rating;
    this.feedbackForm.controls['rating'].setValue( rating );

    const elements = document.querySelectorAll('.rating-item');
    for ( let i = 0; i < elements.length; i++ ) {
      const currentElementvalue = parseInt( elements[i]['dataset'].value, 10 );

      if ( currentElementvalue <= this.selectedRating ) {
        elements[i].classList.add( 'selected' );
      } else {
        elements[i].classList.remove( 'selected' );
      }
    }
  }

  onHover( event, rating ) {
    const item = event.target;
    const elements = document.querySelectorAll('.rating-item');
    for ( let i = 0; i < elements.length; i++ ) {
      const currentElementvalue = parseInt( elements[i]['dataset'].value, 10 );

      if ( currentElementvalue <= rating ) {
        elements[i].classList.add( 'hovered' );
      } else {
        elements[i].classList.remove( 'hovered' );
      }
    }
  }

  onOut ( event, rating ) {
    const item = event.target;

    item.classList.remove( 'hovered' );
    const elements = document.querySelectorAll('.rating-item');

    for ( let i = 0; i < elements.length; i++ ) {
      const currentElementvalue = parseInt( elements[i]['dataset'].value, 10 );

      if ( currentElementvalue > this.selectedRating ) {
        elements[i].classList.remove( 'hovered' );
      }

    }

  }
}
