import { animate, group, query, stagger, state, style, transition, trigger } from '@angular/animations';

export const swTrigger = trigger('swTrigger', [
  transition(':enter', [

    style(
      {
        opacity: 0,
      }),
    animate(
      '0.5s 300ms ease-in',
      style(
        {
          opacity: 1,
        })
    )
  ]),
  transition(':leave', [
    animate(
      '0.3s ease-out',
      style(
        {
          opacity: 0,
        })
    )
  ])
]);
