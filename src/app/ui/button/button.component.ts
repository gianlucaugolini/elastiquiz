import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit {

  @Input() type: string;
  @Input() size: number;
  constructor( private authService: AuthService ) { }

  ngOnInit() {
  }

  onQuit() {
    this.authService.logout()
      .subscribe(
        res => {
          return true;
        },
        error => console.error( error ),
        () => console.log( 'Successfully logged out2! ' )
      );
  }

}
