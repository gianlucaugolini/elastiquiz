import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  authStateSubscription: Subscription;
  isAuthenticated: boolean;
  currentUserName: string;

  constructor( private authService: AuthService, private router: Router ) { }

  ngOnInit() {
    this.authStateSubscription = this
      .authService
      .authChange$
      .subscribe(
        user => {
          if ( user ) {
            this.isAuthenticated = true;
            this.currentUserName = user.name;
          } else {
            this.isAuthenticated = false;
            this.currentUserName = null;
          }
        }
      );
  }

  onQuit() {
    this.authService.logout()
      .subscribe(
        res => {
          return this.router.navigate([ '/home' ] );
        },
        error => console.error( error ),
        () => console.log( 'Successfully logged out! ' )
      );
  }
}
