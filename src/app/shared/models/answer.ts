export class Answer {

  constructor( public text: string, private correct: boolean ) {}

  isCorrect() {
    return this.correct;
  }
}


