import { Answer } from './answer';

export class Question {

  constructor( public question: string, private category: string, private difficulty: string, public answers: Answer[] ) {}

  get points(): number {

    if ( this.difficulty === 'easy' ) {
      return 5;
    } else if ( this.difficulty === 'medium' ) {
      return 10;
    }

    return 15;
  }
  get time(): number {

    if ( this.difficulty === 'easy' ) {
      return 15;
    } else if ( this.difficulty === 'medium' ) {
      return 25;
    }

    return 30;
  }
}
