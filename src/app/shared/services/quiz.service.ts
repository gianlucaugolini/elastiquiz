import { Injectable } from '@angular/core';
import { Question } from '../models/question';
import { Answer } from '../models/answer';
import { DataService } from './data.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { RankingService } from './ranking.service';

@Injectable({
  providedIn: 'root'
})

export class QuizService {

  quiz: Question[];

  quizState = {
    initialized: false,
    started: false,
    running: false,
    completed: false,
    currentQuestion: {
      index: -1,
      time: 0,
      left: 0,
      elapsed: 0,
    },
    score: 0,
  };

  quizState$ = new BehaviorSubject<any>( this.quizState );
  revealAnswers$ = new Subject<boolean>();

  currentPoints$: BehaviorSubject<number> = new BehaviorSubject<number>( 0 );

  private currentQuestion = new Subject<Question>();
  currentQuestion$ = this.currentQuestion.asObservable();

  private interval;

  constructor( private dataService: DataService, private rankingService: RankingService ) {}

  getQuestions() {
    this
      .dataService
      .getQestions()
      .subscribe(
        ( res ) => {
          if ( res ) {
            this.quiz = res;
          }
        },
        error => {
          console.error( error );
        },
        () => {
          this.quizState.initialized = true;
          this.quizState$.next( this.quizState );
        }
      );
  }

  initializeQuiz() {
    this.setInitialQuizState();
    this.getQuestions();
  }

  startQuiz() {
    if ( !this.quizState.started ) {
      this.quizState.started = true;
      this.nextQuestion();
    }
  }

  setInitialQuizState() {
    this.quizState = {
      initialized: false,
      started: false,
      running: false,
      completed: false,
      currentQuestion: {
        index: -1,
        time: 0,
        left: 0,
        elapsed: 0,
      },
      score: 0,
    };
  }

  private nextQuestion() {
    this.resetTimer();
    if ( ++this.quizState.currentQuestion.index < this.quiz.length ) {
      this.revealAnswers$.next( false );
      this.quizState.currentQuestion.time = this.quiz[ this.quizState.currentQuestion.index ].time;
      this.quizState.currentQuestion.elapsed = 0;
      this.quizState.currentQuestion.left = this.quiz[ this.quizState.currentQuestion.index ].time;
      this.quizState.running = true;
      this.currentQuestion.next( null );

      setTimeout( () => {
        this.currentQuestion.next( this.quiz[ this.quizState.currentQuestion.index ] );
        this.startTimer();
      }, 1000 );

    } else {
      this.closeQuiz();
    }
  }

  closeQuiz() {
    this
      .rankingService
      .saveScore( this.quizState.score )
      .subscribe(
        res => {
          if ( res ) {
            this.quizState.completed = true;
            this.quizState$.next( this.quizState );
          } else {
            this.quizState.completed = false;
            this.quizState$.next( this.quizState );
          }
        }
      );
  }

  checkAnswer( answer: Answer ) {
    this.resetTimer();
    const correct = answer.isCorrect();

    if ( correct ) {
      this.addPoints();
    }

    this.revealAnswers$.next( true );

    setTimeout(
      () => {
        this.nextQuestion();
      }, 2000
    );

  }

  addPoints() {
    const time = this.quizState.currentQuestion.left;
    this.quizState.score += this.quiz[ this.quizState.currentQuestion.index ].points * time;
    this.currentPoints$.next( this.quizState.score );
  }

  private startTimer() {
    this.interval = setInterval( () => {
      if ( this.quizState.currentQuestion.left > 0 ) {
        this.quizState.currentQuestion.elapsed++;
        this.quizState.currentQuestion.left = this.quizState.currentQuestion.time - this.quizState.currentQuestion.elapsed;
        this.quizState$.next( this.quizState );
      } else {
        this.nextQuestion();
      }
    }, 1000 );
  }

  private resetTimer() {
    this.quizState.running = false;
    if ( this.interval ) {
      clearInterval( this.interval );
    }
  }

}
