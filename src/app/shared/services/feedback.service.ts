import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, Subject } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthData} from '../../auth/auth-data.model';
import {TokenService} from '../../auth/token.service';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {


  private prodBaseUrl = 'http://elastiquiz-api.pluriahost.it/public/api';
  private devBaseUrl = 'http://localhost:8000/api';

  private endpoints = {
    save      : '/feedback/save'
  };

  constructor( private auth: AuthService, private http: HttpClient, private tokenService: TokenService ) { }

  saveFeedback( rating: number, text: string ) {

    const url = this.prodBaseUrl + this.endpoints.save;
    const options = this.prepareHeaders();
    return this.http.post<any>( url, { rating: rating, text: text }, options );

  }

  private prepareHeaders() {

    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.tokenService.getToken()
      })
    };

  }
}
