import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Question } from '../models/question';
import { map } from 'rxjs/operators';
import { Answer } from '../models/answer';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // private readonly url = 'https://opentdb.com/api.php?amount=5&type=multiple';
  private readonly url = 'https://opentdb.com/api.php?amount=5&difficulty=easy&type=multiple';

  constructor( private http: HttpClient ) {}

  getQestions(): Observable<Question[]> {
    return this.http
      .get( this.url, {})
      .pipe(
        map(
          ( response ) => {
            const questions = [];

            response['results'].map(
              ( item ) => {

                const answers = [];

                answers.push( new Answer( item.correct_answer, true ) );

                item.incorrect_answers.forEach(
                  ( el ) => {
                    answers.push( new Answer( el, false ) );
                  }
                );

                const question = new Question(
                  item.question,
                  item.category,
                  item.difficulty,
                  this.shuffle(answers)
                );

                questions.push( question );
              }
            );
            return questions;
          }
        )
      );
  }

  sendResults( points: number ) {
    console.log( '' );
  }

  shuffle( a ) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  /*
  postAnswer(answer: Answering): Observable<any> {
    return this.http
      .post('/answer', answer)
  }*/

}
