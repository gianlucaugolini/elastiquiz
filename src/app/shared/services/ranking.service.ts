import { Injectable } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TokenService } from '../../auth/token.service';

@Injectable({
  providedIn: 'root'
})
export class RankingService {


  private prodBaseUrl = 'http://elastiquiz-api.pluriahost.it/public/api';
  private devBaseUrl = 'http://localhost:8000/api';

  private endpoints = {
    save      : '/quiz/save',
    ranking   : '/ranking/all'
  };

  constructor( private auth: AuthService, private http: HttpClient, private tokenService: TokenService ) { }

  fetchScores() {

    const url = this.prodBaseUrl + this.endpoints.ranking;
    const options = this.prepareHeaders();
    return this.http.get<any>( url, options );
  }

  saveScore( score: number ) {

    const url = this.prodBaseUrl + this.endpoints.save;
    const options = this.prepareHeaders();
    return this.http.post<any>( url, { score: score }, options );

  }

  private prepareHeaders() {

    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json',
        'Authorization': 'Bearer ' + this.tokenService.getToken()
      })
    };

  }
}
