import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { QuizService } from '../services/quiz.service';

@Injectable()
export class HasCompletedGuard implements CanActivate {
  constructor( private quizService: QuizService, private router: Router ) { }

  canActivate( next: ActivatedRouteSnapshot, state: RouterStateSnapshot ) {
    if ( this.quizService.quizState.completed ) {
      return true;
    }
    this.router.navigate(['/start'] );
    return false;
  }
}
