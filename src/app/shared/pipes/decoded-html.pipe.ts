import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decodedHtml'
})
export class DecodedHtmlPipe implements PipeTransform {
  transform( value: string ) {
    if ( value ) {

      const tempElement = document.createElement('div');
      tempElement.innerHTML = value;
      return tempElement.innerText;

    }

    return null;
  }

}
