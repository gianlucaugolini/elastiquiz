import {Component, OnDestroy, OnInit} from '@angular/core';
import { Router } from '@angular/router';
import { QuizService } from '../shared/services/quiz.service';
import {Subscription} from 'rxjs';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss']
})
export class StartComponent implements OnInit, OnDestroy {

  quizStateSubscription: Subscription;
  quizInitialized = false;
  userName: string;

  constructor( private quizService: QuizService, private authService: AuthService, private router: Router ) {
    this.quizStateSubscription = this
      .quizService
      .quizState$
      .subscribe(
        res => {
          if ( res ) {
            this.quizInitialized = res.initialized;
          }
        }
      );
  }

  ngOnInit() {
    this
      .quizService
      .initializeQuiz();

    this.userName = this.authService.getUser().name;
  }

  ngOnDestroy(): void {
    this.quizStateSubscription.unsubscribe();
  }

  onStart() {
    if ( this.quizInitialized ) {
      return this.router.navigate( [ '/quiz' ] );
    }
  }

}
