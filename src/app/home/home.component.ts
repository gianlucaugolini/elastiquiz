import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  authStateSubscription: Subscription;
  isAuthenticated: boolean;

  constructor( private authService: AuthService ) { }

  ngOnInit() {
    this.authStateSubscription = this
      .authService
      .authChange$
      .subscribe(
        user => {
          if ( user ) {
            this.isAuthenticated = true;
          } else {
            this.isAuthenticated = false;
          }
        }
      );
  }

}
