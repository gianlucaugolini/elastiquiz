import {animate, group, query, stagger, style, transition, trigger} from '@angular/animations';

export const anwserStaggeredTrigger = trigger('anwserStaggeredTrigger', [
  transition('* => *', [
    group([
      query(
        ':enter',
        [
          style({ display: 'none', opacity: 0, transform: 'translateX( -100% )' } ),
        ], { optional: true })
    ]),
    group([
      query(
        ':leave',
        [
          style({ display: 'block', opacity: 1, transform: 'translateX( 0% )' } ),
          stagger(
            '50ms',
            animate(
              '300ms ease-out',
              style({ display: 'none', opacity: 0, transform: 'translateX( 100% )' } ),
            )
          )
        ], { optional: true }
      )
    ]),
    group([
      query(
        ':enter',
        [
          style({ display: 'block', opacity: 0, transform: 'translateX( -100% )' } ),
        ], { optional: true }),
      query(
        ':enter',
        [
          stagger(
            '50ms',
            animate(
              '300ms ease-out',
              style({ opacity: 1, transform: 'translateX( 0% )' } )
            )
          )
        ], { optional: true }
      )
    ]),
  ])
]);
