import { animate, group, keyframes, state, style, transition, trigger } from '@angular/animations';


export const anwserTrigger = trigger('anwserTrigger', [
  state(
    'hidden',
    style(
      {
        backgroundColor: 'darkgrey'
      })
  ),
  state(
    'incorrect',
    style(
      {
        backgroundColor: 'lightcoral'
      })
  ),
  state(
    'correct',
    style(
      {
        backgroundColor: 'lightgreen'
      })
  )
]);
