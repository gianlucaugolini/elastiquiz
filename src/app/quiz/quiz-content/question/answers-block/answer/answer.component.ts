import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Answer } from '../../../../../shared/models/answer';
import { QuizService } from '../../../../../shared/services/quiz.service';
import { Subscription } from 'rxjs';
import { anwserTrigger } from './animations';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.scss'],
  animations: [
    anwserTrigger
  ]
})
export class AnswerComponent implements OnInit, OnDestroy {

  @Input() answer: Answer;
  answerClass = 'hidden';
  canAnswer = true;
  answerSubscription: Subscription;

  constructor( private quizService: QuizService ) { }

  ngOnInit() {
    this.answerSubscription = this
      .quizService
      .revealAnswers$
      .subscribe(
        ( reveal ) => {
          if ( reveal ) {
            this.canAnswer = false;
            this.answerClass = this.answer.isCorrect() ? 'correct' : 'wrong';
          } else {
            this.canAnswer = true;
            this.answerClass = '';
          }
        }
      );
  }

  selectAnswer() {
    if ( this.canAnswer ) {
      this.canAnswer = false;
      this.quizService.checkAnswer( this.answer );
    }
  }

  ngOnDestroy(): void {
    this
      .answerSubscription
      .unsubscribe();
  }
}
