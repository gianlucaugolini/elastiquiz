import { Component, Input, OnInit } from '@angular/core';
import { Answer } from '../../../../shared/models/answer';
import { anwserStaggeredTrigger } from './animations';

@Component({
  selector: 'app-answers-block',
  templateUrl: './answers-block.component.html',
  styleUrls: ['./answers-block.component.scss'],
  animations: [
    anwserStaggeredTrigger
  ]
})
export class AnswersBlockComponent implements OnInit {

  @Input() answers: Answer[];
  constructor() { }

  ngOnInit() {
  }

}
