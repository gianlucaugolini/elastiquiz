import { Component, Input, OnInit } from '@angular/core';
import { Question } from '../../shared/models/question';

@Component({
  selector: 'app-quiz-content',
  templateUrl: './quiz-content.component.html',
  styleUrls: ['./quiz-content.component.scss']
})
export class QuizContentComponent implements OnInit {

  @Input() question: Question[];

  constructor() { }

  ngOnInit() {}
}
