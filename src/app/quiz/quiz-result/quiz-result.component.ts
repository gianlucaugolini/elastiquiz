import { Component, OnInit } from '@angular/core';
import { QuizService } from '../../shared/services/quiz.service';

@Component({
  selector: 'app-quiz-result',
  templateUrl: './quiz-result.component.html',
  styleUrls: ['./quiz-result.component.scss']
})
export class QuizResultComponent implements OnInit {

  score = 0;

  constructor( private quizService: QuizService ) { }

  ngOnInit() {
    this.score = this.quizService.quizState.score;
  }
}
