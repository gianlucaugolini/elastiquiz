import { animate, group, keyframes, state, style, transition, trigger } from '@angular/animations';

export const showStateTrigger = trigger('showState', [
  transition(':enter', [
    style({
      opacity: 0
    }),
    animate( '{{time}}' )
  ], { params : { time: '300ms' } } ),
  transition(
    ':leave',
    animate(
      300,
      style({
        opacity: 0
      })
    )
  )
]);

export const progressAnimation = trigger('progressAnimation', [
  state(
    'timeEnd',
    style( {
      width: '0%',
    })
  ),
  state(
    'timeFull',
    style({
      width: '100%',
      backgroundColor: 'lightgreen',
    })
  ),
  transition(
    '* => timeEnd',
    [
      group( [
        animate(
          '15s',
          keyframes(
            [
              style({
                backgroundColor: 'lightgreen',
                width: '100%'
              }),
              style({
                backgroundColor: 'orange',
              }),
              style({
                backgroundColor: 'red',
                width: '0%'
              }),
            ])
        ),
        animate(
          '15s',
          keyframes(
            [
              style({
                width: '100%'
              }),
              style({
                width: '0%'
              }),
            ])
        )
      ] )
    ]
  ),
  transition(
    '* => timeFull',
    [
        animate( '500ms')
    ]
  )
]);
