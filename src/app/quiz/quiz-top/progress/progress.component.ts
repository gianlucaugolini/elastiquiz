import {Component, OnInit, OnChanges, SimpleChanges, SimpleChange, Input } from '@angular/core';
import { progressAnimation, showStateTrigger } from './animations';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.scss'],
  animations: [
    showStateTrigger,
    progressAnimation
  ]
})
export class ProgressComponent implements OnInit, OnChanges {

  @Input() time: number;
  @Input() timeLeft: number;
  @Input() animate: boolean;

  currentState;

  constructor() { }

  ngOnInit() { }


  ngOnChanges( changes: SimpleChanges ) {
    this.handleStateChange();
  }

  handleStateChange() {
    if ( this.time === this.timeLeft && this.animate === true ) {
      setTimeout( () => { this.currentState = 'timeEnd'; } );
    } else if ( this.animate === false || this.timeLeft === 0 ) {
      this.currentState = 'timeFull';
    }
  }

}
