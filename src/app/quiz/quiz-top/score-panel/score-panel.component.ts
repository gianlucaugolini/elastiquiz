import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-score-panel',
  templateUrl: './score-panel.component.html',
  styleUrls: ['./score-panel.component.scss']
})
export class ScorePanelComponent implements OnInit {

  @Input() points;
  constructor() { }

  ngOnInit() {
  }

}
