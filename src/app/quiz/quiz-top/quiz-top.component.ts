import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-quiz-top',
  templateUrl: './quiz-top.component.html',
  styleUrls: ['./quiz-top.component.scss'],
})
export class QuizTopComponent implements OnInit {

  @Input() state: any;

  constructor() { }

  ngOnInit() {}
}
