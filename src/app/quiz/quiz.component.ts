import { Component, OnDestroy, OnInit } from '@angular/core';
import { QuizService } from '../shared/services/quiz.service';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit, OnDestroy {

  currentQuestion: any;
  subscription: Subscription;
  quizStateSubscription: Subscription;
  quizState: any;

  constructor( private quizService: QuizService, private router: Router ) {

    this.subscription = this
      .quizService
      .currentQuestion$
      .subscribe(
        item => {
          setTimeout(() => {
            this.currentQuestion = { ...item };
          });
        },
            error =>  this.router.navigate( [ '/not-found' ] ),
            () => console.log('QuizComponent Subscription completed')
      );

    this.quizStateSubscription = this
      .quizService
      .quizState$
      .subscribe(
        res => {
          if ( res ) {
            this.quizState = res;
            this.handleState( this.quizState );
          }

        },
          error =>  this.router.navigate( [ '/not-found' ] ),
        () => console.log('QuizComponent Subscription completed')
      );
  }

  ngOnInit() {
    this
      .quizService
      .startQuiz();
  }

  ngOnDestroy(): void {
    this
      .subscription
      .unsubscribe();

    this
      .quizStateSubscription
      .unsubscribe();
  }

  onCompleteQuiz() {
    return this
      .router
      .navigate( [ '/quiz-result' ] );
  }

  handleState( state: any ) {

    if ( state.completed ) {
      this.onCompleteQuiz();
    }
  }


}
