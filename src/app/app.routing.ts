import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomeComponent } from './home/home.component';
import { QuizComponent } from './quiz/quiz.component';
import { RankingComponent } from './ranking/ranking.component';
import { StartComponent } from './start/start.component';
import { CanPlayGuard } from './shared/guards/can-play.guard';
import { QuizResultComponent } from './quiz/quiz-result/quiz-result.component';
import { HasCompletedGuard } from './shared/guards/has-completed.guard';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { AuthGuard } from './auth/auth.guard';
import { LoggedInGuard } from './auth/logged-in.guard';
import {GuestBookComponent} from './guest-book/guest-book.component';

const routes: Routes = [
  { path: '',  redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [
      LoggedInGuard
    ]
},
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [
      LoggedInGuard
    ]
  },
  {
    path: 'start',
    component: StartComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'quiz',
    component: QuizComponent,
    canActivate: [
      AuthGuard,
      CanPlayGuard
    ]
  },
  {
    path: 'quiz-result',
    component: QuizResultComponent,
    canActivate: [
      AuthGuard,
      HasCompletedGuard
    ]
  },
  {
    path: 'ranking',
    component: RankingComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'leave-a-feedback',
    component: GuestBookComponent,
    canActivate: [
      AuthGuard
    ]
  },
  {
    path: 'not-found',
    component: NotFoundComponent
  },
  {
    path: '**',
    redirectTo: 'not-found'
  },
];

export const routingModule: ModuleWithProviders = RouterModule.forRoot( routes );

